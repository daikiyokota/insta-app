User.create!(name:  "Example User",
             nickname: "foobar",
             password:              "foobar",
             password_confirmation: "foobar")

99.times do |n|
  name  = Faker::Name.name
  nickname = "example-#{n+1}"
  password = "password"
  User.create!(name:  name,
               nickname: nickname,
               password:              password,
               password_confirmation: password)
end