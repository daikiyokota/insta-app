class LikesController < ApplicationController
  before_action :logged_in_user, only: [:create, :show, :destroy]
  #before_action :correct_user,   only: :destroy
  
  def create 
    @like = Like.new(user_id: current_user.id, micropost_id: params[:id])
    @like.save
    flash[:success] = "お気に入りに追加しました。"
    micropost = Micropost.find_by(id: params[:id])
    Alert.create(user_id: micropost.user_id.to_i, content: "#{current_user.name}があなたの投稿にいいねしました。")
    redirect_to root_url
  end
  
  def show
    @likes = Like.where(user_id: current_user.id)
    #pagenationどうやってかけるんだ？保留
    @microposts = []
    @likes.each do |like|
      micropost = Micropost.find_by(id: like.micropost_id)
      @microposts << micropost
    end      
  end 
  
  def destroy
    @like = Like.find_by(user_id: current_user.id, micropost_id: params[:id])
    if @like 
      @like.destroy
      flash[:success] = "お気に入りから削除しました。"
      redirect_to root_url
    else
      flash[:danger] = "削除できませんでした。"
      redirect_to root_url
    end 
  end 
  
end
