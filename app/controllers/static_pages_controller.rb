class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      @comment    = current_user.comments.build
    end
  end
  
  def policy
  end 
  
  private
    def search_params
      params.require(:q).permit(:name_cont)
    end
end
