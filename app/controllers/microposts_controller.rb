class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:index, :new, :create, :destroy]
  before_action :correct_user,   only: :destroy
  
  def new
    @micropost = current_user.microposts.build(micropost_params)
  end
  
  def index
    @q = Micropost.all.ransack(params[:q])
    @microposts = @q.result(distinct: true)
  end
  
  def show
   @user = current_user
   @micropost = Micropost.find(params[:id])
   @comment = current_user.comments.build
   @comments = Comment.where(micropost_id: @micropost.id)
  end 
  
  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "投稿に成功しました!"
      redirect_to root_url
    else
      @feed_items = current_user.feed.paginate(page: params[:page])
      render 'static_pages/home'
    end
  end
  
  def destroy
    @micropost.destroy
    flash[:success] = "投稿を削除しました。"
    redirect_to request.referrer || root_url
  end
  
  private

    def micropost_params
      params.fetch(:micropost, {}).permit(:picture)
    end
    
    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end 
end
