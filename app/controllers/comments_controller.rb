class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  
  
  def create
    #なぜか"micropost_id"=>"24", "comment"=>{"content"=>"aaaaaaaaaaaa"}, "commit"=>"Post"}となってしまう。保留。
    @comment = current_user.comments.build(comment_params)
    @comment.micropost_id = params[:micropost_id]
    
    if @comment.save
      flash[:success] = "コメントしました！"
      micropost = Micropost.find_by(id: params[:micropost_id])
      Alert.create(user_id: micropost.user_id.to_i, content: "#{current_user.name}があなたの投稿にコメントしました。")
      redirect_to root_url
    else
      flash[:danger] = "コメントができませんでした。"
      redirect_to root_url
    end 
  end 
  
  def destroy
    @comment = Comment.find_by(id: params[:id])
    @comment.destroy
    flash[:success] = "コメントを削除しました。"
    redirect_to root_url
  end
  
  private

    def comment_params
      params.require(:comment).permit(:content)
    end
    
    def correct_user
      @comment = Comment.find_by(id: params[:id])
      @user = current_user
      unless current_user?(@comment.user)
        flash[:danger] = "コメントを削除できません"
        redirect_to root_url
      end 
    end
end
