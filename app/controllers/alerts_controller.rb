class AlertsController < ApplicationController
  before_action :logged_in_user, only: [:show, :destroy]
  before_action :correct_user,   only: [:destroy]
  before_action :right_user,     only: [:show]
  
  def show
    @alerts = Alert.where(user_id: current_user.id)
  end
  
  def destroy
    @alert = Alert.find_by(id: params[:id])
    @alert.destroy
    flash[:success] = "通知を削除しました。"
    redirect_to root_url
  end 
end

private
  # 正しいユーザーかどうか確認
    def correct_user
      @alert = Alert.find_by(id: params[:id])
      @user = User.find_by(id: @alert.user_id)
      redirect_to(root_url) unless current_user?(@user)
    end
    
    def right_user
      @user = User.find_by(id: params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end 