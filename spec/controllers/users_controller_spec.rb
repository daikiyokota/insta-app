require 'rails_helper'
RSpec.describe UsersController, type: :controller do
  render_views
  let(:base_title) { 'Insta App' }
  describe "#index" do
    context "ログインユーザーの場合" do
      before do
        @user = FactoryBot.create(:user)
        log_in_as(@user)
      end
      
      it "レスポンスを正常に返す" do
        get :index
        aggregate_failures do
          expect(response).to be_success
          expect(response).to have_http_status "200"
          assert_select "title", "All users | #{base_title}"
        end 
      end 
    end

    context "ゲストの場合" do
      before do
        @user = FactoryBot.create(:user)
      end 
      
      it "レスポンスは302が返る" do
        get :index
        expect(response).to have_http_status "302"
      end 
      
      it "ログイン画面にリダイレクトされる" do
        get :index
        expect(response).to redirect_to "/login"
      end 
    end
  end
  
  
  describe "#new" do
    it "レスポンスを正常に返す" do
      get :new
      expect(response).to have_http_status(:success)
      assert_select "title", "Sign up | #{base_title}"
    end
  end
  
  describe "#show" do
    context "ログインユーザーの場合" do
      before do
        @user = FactoryBot.create(:user)
        log_in_as(@user)
      end 
      
      it "正常にレスポンスを返す" do
        get :show, params: {id: @user.id }
        expect(response).to have_http_status(:success)
        assert_select "title", "#{@user.name} | #{base_title}"
      end 
    end 
    
    context "ゲストの場合" do
      before do
        @user = FactoryBot.create(:user)
      end 
      
      it "ログイン画面にリダイレクトされる" do
        get :show, params: {id: @user.id }
        expect(response).to redirect_to "/login"
      end
    end 
  end 
  
  describe "#edit" do
    before do
      @user = FactoryBot.create(:user)
      log_in_as(@user)
    end 
    
    context "適切なユーザーの場合" do 
      it "正常にレスポンスを返す" do 
        get :edit, params: { id: @user.id }
        expect(response).to be_success
        assert_select "title", "Edit user | #{base_title}"
      end 
    end 
    
    context "不適切なユーザーの場合" do
      before do
        @other_user = FactoryBot.create(:user, :foobar)
        log_in_as(@other_user)
      end 
      
      it "302レスポンスを返すこと" do
        get :edit, params: { id: @user.id }
        expect(response).to have_http_status "302"
      end 
    end
    
    context "ゲストの場合" do
      before do
        log_out_as(@user)
        @other_user = FactoryBot.create(:user, :yokota)
      end 
      it "フラッシュメッセージが表示され、ログイン画面にリダイレクトされる" do
        get :edit, params: { id: @other_user.id }
        expect(flash).to_not be_blank
        expect(response).to redirect_to "/login"
      end 
    end 
  end
  
  describe "#create" do
    
    context "ユーザーを作成する場合" do 
      it "ユーザーを作成したら数が増える" do
        expect{
          post :create, params: {user: { name: "yokota", nickname: "yokota", password: "yokota", 
                                password_confirmation: "yokota"}}
        }.to change{ User.count }.by(1)
        expect(flash).to_not be_blank
      end 
    end 
  end
  
  describe "#update" do
    
    context "適切なユーザーの場合" do 
      before do
        @user = FactoryBot.create(:user)
        log_in_as(@user)
      end 
      
      it "ユーザーの名前を更新できる" do 
        patch :update, params: {id: @user.id, user: { name: "foobar", nickname: "foobar", password: "hogehoge", 
                                password_confirmation: "hogehoge"}}
        expect(@user.reload.name).to eq "foobar" 
      end 

      it "showアクションのテンプレートにリダイレクトされる" do
        user_params = FactoryBot.attributes_for(:user, name: "yokota")
        patch :update, params: {id: @user.id, user: user_params}
        expect(response).to redirect_to "/users/#{@user.id}"
        expect(@user.reload.name).to eq "yokota" 
      end 
    end
    
    context "正しくないユーザーの場合" do
      before do
        @user = FactoryBot.create(:user)
        @other_user = FactoryBot.create(:user, :foobar)
      end 
      
      it "別の人をupdateしようとするとroot_urlにリダイレクトされる" do
        log_in_as(@other_user)
        user_params = FactoryBot.attributes_for(:user, name: "yokota")
        patch :update, params: {id: @user.id, user: user_params}
        expect(response).to redirect_to root_url
      end 
    end 
        
    context "ゲストの場合" do
      before do 
        @user = FactoryBot.create(:user)
      end
      
      it "フラッシュメッセージが表示され、ログインページにリダイレクトされる" do
        user_params = FactoryBot.attributes_for(:user, name: "yokota")
        patch :update, params: {id: @user.id, user: user_params}
        expect(flash).to_not be_blank
        expect(response).to redirect_to login_url
      end 
    end 
  end 
end
