require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  
  describe "#new" do
    it "ログインページが正しく表示される" do
      get :new
      expect(response).to be_success
    end 
  end 
  
  describe "#destroy" do
    before do
     @user = FactoryBot.create(:user)
    end 
    
    it "複数のブラウザでログアウトをした時" do
      log_in_as(@user)
      delete :destroy
      expect(response).to redirect_to root_url
      delete :destroy
      expect(response).to redirect_to root_url
    end 
  end 
end
