require 'rails_helper'

RSpec.describe MicropostsController, type: :controller do
  render_views
  describe "#new" do
    before do
      @user = FactoryBot.create(:user)
    end 
    it "ログインしていると正常に表示される" do
      log_in_as(@user)
      get :new
      expect(response).to be_success
      expect(response).to have_http_status "200"
      assert_select "title", "Post | Insta App"
    end 
    
    it "ログインしてないとログインページにリダイレクトされる" do
      get :new
      expect(response).to have_http_status "302"
      expect(response).to redirect_to "/login"
    end 
  end 
end
