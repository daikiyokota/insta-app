require 'rails_helper'

RSpec.describe AlertsController, type: :controller do
  render_views
  
  before do
    @user = FactoryBot.create(:user)
    @alert = FactoryBot.create(:alert, user_id: @user.id)
  end
  
  describe "#show" do
    context "ログインユーザーの場合" do 
      it "ログインユーザーの場合正常にレスポンスを返す" do
        log_in_as(@user)
        get :show, params: {id: @user.id}
        expect(response).to be_success
        expect(response).to have_http_status "200"
        assert_select "title", "Alerts | Insta App"
      end
    end 
    
    context "ゲストの場合" do
      it "ゲストの場合login_urlにリダイレクトされる" do
        get :show, params: {id: @user.id}
        expect(response).to have_http_status "302"
        expect(response).to redirect_to "/login"
      end
    end 
  end
  
  describe "#destroy" do
    context "ログインしていて適切なユーザーの場合" do
      it "通知を削除することができる" do
        log_in_as(@user)
        expect{
          delete :destroy, params: {id: @alert.id}
        }.to change{ Alert.count }.by(-1)
        expect(response).to redirect_to "/"
      end 
    end
    
    context "ログインしている不適切なユーザーの場合" do
      it "root_urlにリダイレクトされる" do
        @other_user = FactoryBot.create(:user, :foobar)
        log_in_as(@other_user)
        expect{
          delete :destroy, params: {id: @alert.id}
        }.to change{ Alert.count }.by(0)
        expect(response).to redirect_to "/"
      end 
    end 
  end 
end
