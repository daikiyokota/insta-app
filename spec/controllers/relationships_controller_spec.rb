require 'rails_helper'

RSpec.describe RelationshipsController, type: :controller do
  describe "#create" do
    before do
        @user = FactoryBot.create(:user)
        @second_user = FactoryBot.create(:user, :foobar)
    end
    
    context "ログインユーザーの場合" do
      it "createアクションを呼び出すとRelationshipが1増える" do
        log_in_as(@user)
        expect{
          post :create, params: {followed_id: @second_user.id }
        }.to change{Relationship.count}.by(1)
      end 
    end 
    
    context "ゲストの場合" do 
      it "ログインせずにcreateアクションを呼び出すとログインページにリダイレクトされる" do
        expect{
          post :create
        }.to change{Relationship.count}.by(0)
        expect(response).to redirect_to login_url
      end
    end 
  end 
  
  describe "#destroy" do
    before do
      @user = FactoryBot.create(:user)
      @second_user = FactoryBot.create(:user, :foobar)
      @relationship = FactoryBot.create(:relationship)
    end
    
    context "ログインユーザーの場合" do
      it "destroyアクションを呼び出すとRelationshipが1減る" do
        log_in_as(@user)
        expect{
          delete :destroy, params: { id: @relationship.id}
        }.to change{Relationship.count}.by(-1)
      end 
    end 
    
    context "ゲストの場合" do
      it "ログインせずにdestroyアクションを呼び出すとログインページにリダイレクトされる" do
        expect{
          delete :destroy, params: { id: @relationship.id}
        }.to change{Relationship.count}.by(0)
        expect(response).to redirect_to login_url
      end 
    end 
  end
end
