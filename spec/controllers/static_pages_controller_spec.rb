require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  render_views
  let(:base_title) { 'Insta App' }
  
  describe "#home" do
    it "titleがInsta Appとなっている" do
      get :home
      assert_select "title", "#{base_title}"
    end 
  end
  
  describe "#policy" do
    it "正常にレスポンスを返す" do
      get :policy
      expect(response).to have_http_status "200"
      expect(response).to be_success
      assert_select "title", "Policy | #{base_title}"
    end 
  end 
end 