module InstaHelper
  
  # テストユーザーとしてログインする
  def log_in_as(user)
    session[:user_id] = user.id
  end
  
  #テストユーザーがログアウトをする
  def log_out_as(user)
    session[:user_id] = nil
  end
  
  #フィーチャーテストでログインする
  def log_in_as_foobar
    visit root_url
    click_link "ログイン"
    fill_in "ニックネーム", with: "foobar"
    fill_in "パスワード", with: "foobar"
    click_button "ログイン"
  end
  
end 