require 'rails_helper'

RSpec.describe User, type: :model do
  it "nameとが無いと登録できない" do
    user = User.new(name: " ", nickname: "hogehoge")
    user.valid?
    expect(user.errors[:name]).to include("can't be blank")
  end
  
  it "nicknameとが無いと登録できない" do
    user = User.new(name: "hogehoge", nickname: " ")
    user.valid?
    expect(user.errors[:nickname]).to include("can't be blank")
  end 
  
  it "パスワードが5文字以下だと登録できない" do 
    user = FactoryBot.build(:user, password: "fooba")
    user.valid?
    expect(user.errors[:password]).to include("is too short (minimum is 6 characters)")
  end 
  
  it "自己紹介が141文字以上だと登録できない" do
    user = FactoryBot.build(:user, introduction: "a" * 141)
    user.valid?
    expect(user.errors[:introduction]).to include("is too long (maximum is 140 characters)")
  end
  
  it "フォローメソッドとアンフォローメソッドが適切に動く" do
    hogehoge = FactoryBot.create(:user)
    foobar = FactoryBot.create(:user, :foobar)
    #フォローしてない場合。
    expect(hogehoge.following?(foobar)).to eq false
    hogehoge.follow(foobar)
    #フォローすると、追加される？
    expect(hogehoge.following?(foobar)).to eq true
    expect(foobar.followers.include?(hogehoge)).to eq true
    #フォローを外すと？
    hogehoge.unfollow(foobar)
    expect(hogehoge.following?(foobar)).to eq false
  end
  
  #なぜかテスト通らない。一旦保留
  # it "feedには適切な投稿が表示されている" do 
  #   hogehoge = FactoryBot.create(:user)
  #   foobar = FactoryBot.create(:user, :foobar)
  #   yokota = FactoryBot.create(:user, :yokota)
  #   #各ユーザーの投稿を作成する
  #   users = [hogehoge, foobar, yokota]
  #   n = 0 
  #   users.each do |user|
  #     user.microposts.create(picture: "test#{n}.jpg")
  #     n += 1
  #   end 
  #   hogehoge.follow(foobar)
  #   # フォローしているユーザーの投稿を確認
  #   foobar.microposts.each do |post_following|
  #     expect(hogehoge.feed).to include(post_following)
  #   end
  #   # 自分自身の投稿を確認
  #   hogehoge.microposts.each do |post_self|
  #     expect(hogehoge.feed).to include(post_self)
  #   end
    
  #   # フォローしていないユーザーの投稿を確認
  #   yokota.microposts.each do |post_unfollowed|
  #     expect(hogehoge.feed).to_not include(post_unfollowed)
  #   end
  # end 
end
