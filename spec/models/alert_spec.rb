require 'rails_helper'

RSpec.describe Alert, type: :model do
  before do
    @user = FactoryBot.create(:user)
  end 
  it "contentが無いと登録できない" do
    alert = @user.alerts.build(content: "")
    alert.valid?
    expect(alert.errors[:content]).to include("can't be blank")
  end
  
  it "user_idが無いと登録できない" do
    alert = Alert.new(content: "This is a test")
    alert.valid?
    expect(alert.errors[:user_id]).to include("can't be blank")
  end
end
