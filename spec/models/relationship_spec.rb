require 'rails_helper'

RSpec.describe Relationship, type: :model do
  before do
    @user_a = FactoryBot.create(:user)
    @user_b = FactoryBot.create(:user, :foobar)
    @relationship = Relationship.new(follower_id: @user_a.id,
                                     followed_id: @user_b.id)
  end
  
  it "@relationshipは有効である" do
    expect(@relationship).to be_valid 
  end 
  
  it "follower_idがない場合は無効になる" do 
    @relationship.follower_id = nil
    expect(@relationship).to_not be_valid 
  end 
    
  
  it "followed_idがない場合は無効になる" do
    @relationship.followed_id = nil
    expect(@relationship).to_not be_valid 
  end 
end
