FactoryBot.define do
  factory :micropost do
    picture "test.jpg"
    association :user
  end
end
