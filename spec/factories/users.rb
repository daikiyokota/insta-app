FactoryBot.define do
  factory :user do
    name "hogehoge"
    nickname "hogehoge"
    password "hogehoge"
    password_confirmation "hogehoge"
    
    trait :foobar do
      name "foobar"
      nickname "foobar"
      password "foobar"
      password_confirmation "foobar"
    end
    
    trait :yokota do
      name "yokota"
      nickname "yokota"
      password "yokota"
      password_confirmation "yokota"
    end 
  end
end
