require 'rails_helper'

RSpec.feature "Following", type: :feature do
  before do
    @user = FactoryBot.create(:user, :foobar)
    @other_user = FactoryBot.create(:user)
    FactoryBot.create(:relationship, follower_id: @user.id, followed_id: @other_user.id)
    FactoryBot.create(:relationship, follower_id: @other_user.id, followed_id: @user.id)
    log_in_as_foobar
    visit root_url
  end 
  
  it "followin_pageが正しく表示されている" do
    click_link "#{@user.following.count} 人をフォロー中"
    expect(page).to have_current_path "/users/#{@user.id}/following"
    expect(@user.following).to_not eq nil
    @user.following.each do |user|
      expect(page).to have_link user.name
    end 
  end
  
  it "followers_pageが正しく表示されている" do
    click_link "フォロワー #{@user.followers.count} 人"
    expect(page).to have_current_path "/users/#{@user.id}/followers"
    expect(@user.followers).to_not eq nil
    @user.followers.each do |user|
      expect(page).to have_link user.name
    end 
  end
  
  it "UnfollowボタンとFollowボタンが機能している" do
    expect{
      click_link "#{@user.following.count} 人をフォロー中"
      click_link "#{@other_user.name}"
      click_button 'フォローを解除する'
    }.to change{ Relationship.count }.by(-1)
    
    #followボタン
    expect{
      click_button 'フォローする'
    }.to change{ Relationship.count }.by(1)
  end
end
