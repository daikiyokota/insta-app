require 'rails_helper'

RSpec.feature "UsersEdit", type: :feature do
  before do
    @user = FactoryBot.create(:user)
    @foobar = FactoryBot.create(:user, :foobar)
  end
  
  it "適切なユーザーの場合アカウント削除のリンクがある" do
    log_in_as_foobar
    find('.dropdown-toggle').click
    click_link "プロフィール設定"
    expect(page).to have_current_path "/users/#{@foobar.id}/edit"
    expect{
      click_link "アカウントを削除する"
    }.to change{ User.count }.by(-1)
    expect(page).to have_current_path "/"
  end 
  
#   it "編集機能で誤った情報を入力すると、編集ページに戻される" do
    
#     log_in_as_foobar
#     find('.dropdown-toggle').click
#     click_link "プロフィール設定"
#     expect(page).to have_current_path "/users/#{@user.id}/edit"
#     fill_in "名前", with: " "
#     fill_in "ニックネーム", with: "　"
#     click_button "プロフィールを変更する"
#     expect(page).to have_current_path "/users/#{@user.id}"
#     expect(page).to have_content "Name can't be blank"
#     expect(page).to have_content "Nickname can't be blank"
#   end
  
#   it "friendly forwardingが機能している" do
#     visit edit_user_path(@user)
#     expect(page).to have_current_path "/login"
#     log_in_as_foobar
#     expect(page).to have_current_path "/users/#{@user.id}/edit"
#     name = "foohoge"
#     nickname = "foohoge"
#     fill_in "名前", with: name
#     fill_in "ニックネーム", with: nickname
#     click_button "プロフィールを変更する"
#     expect(page).to have_current_path "/users/#{@user.id}"
#     expect(@user.reload.name).to eq name
#     expect(@user.email).to eq email
#   end
# NoMethodError:
#       undefined method `size' for nil:NilClass
       
#       このエラーが解決できないから一旦保留
 end


