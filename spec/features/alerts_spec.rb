require 'rails_helper'

RSpec.feature "Alerts", type: :feature do
  before do
    @user = FactoryBot.create(:user, :foobar)
    @alert = FactoryBot.create(:alert, user_id: @user.id)
    @other_user = FactoryBot.create(:user)
    @other_alert = FactoryBot.create(:alert, user_id: @other_user.id)
  end 
  
  it "ログインしていて適切なユーザーの場合削除リンクがある" do
    log_in_as_foobar
    visit root_url
    expect(page).to have_current_path "/"
    #クリックできないから一旦以下で。
    visit alert_url(@user)
    expect(page).to have_current_path "/alerts/#{@user.id}"
    expect{
      click_link "削除する"
    }.to change{ Alert.count }.by(-1)
    expect(page).to have_current_path "/"
  end 
  
  it "ログインしている不適切なユーザーの場合リダイレクトされる" do
    log_in_as_foobar
    visit root_url
    expect(page).to have_current_path "/"
    #クリックできないから一旦以下で。
    visit alert_url(@other_user)
    expect(page).to have_current_path "/"
  end 
  
  it "ゲストの場合リダイレクトされる" do
    visit root_url
    expect(page).to have_current_path "/"
    #クリックできないから一旦以下で。
    visit alert_url(@user)
    expect(page).to have_current_path "/login"
  end 
end
