require 'rails_helper'

RSpec.feature "UsersSignup", type: :feature do
  it "間違った情報で新規登録できない" do
    expect {
      visit root_url
      click_link "新規登録"
      fill_in "名前", with: ""
      fill_in "ニックネーム", with: "　"
      fill_in "パスワード", with: "hoge"
      fill_in "パスワード（確認用）", with: "hoge"
      click_button "アカウントを作成する"
    }.to change{User.count}.by(0)
    #newにrenderされる
    expect(page).to have_current_path "/users"
    expect(page).to have_selector "div#error_explanation"
    expect(page).to have_selector "div.field_with_errors"
  end
  
  it "正しい情報だと新規登録できる" do
    expect {
      visit root_url
      click_link "新規登録"
      fill_in "名前", with: "hogehoge"
      fill_in "ニックネーム", with: "hogehoge"
      fill_in "パスワード", with: "hogehoge"
      fill_in "パスワード（確認用）", with: "hogehoge"
      click_button "アカウントを作成する"
    }.to change{ User.count }.by(1)
    expect(page).to have_current_path "/users/1"
  end
end
