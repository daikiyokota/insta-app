require 'rails_helper'

RSpec.feature "UsersProfile", type: :feature do
   before do
    @user = FactoryBot.create(:user, :foobar)
  end 
  
  it "userのプロフィールページが適切に表示されてる" do
    log_in_as_foobar
    visit "/users/#{@user.id}"
    expect(page).to have_current_path "/users/#{@user.id}"
    expect(page).to have_selector 'h1', text: @user.name
    expect(page).to have_selector 'h1', text: @user.nickname
    expect(page).to have_selector 'p', text: @user.introduction
    expect(page).to have_content @user.microposts.count.to_s
  end 
end
