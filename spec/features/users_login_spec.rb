require 'rails_helper'

RSpec.feature "UsersLogin", type: :feature do
  before do
    @user = FactoryBot.create(:user, :foobar)
  end 
  
  it "ログインからログアウトまで" do
    log_in_as_foobar
    expect(@user.reload.remember_digest).to eq nil
    expect(page).to have_current_path "/users/#{@user.id}"
    expect(page).to have_link 'ログイン', count: 0
    expect(page).to have_link 'ログアウト'
    expect(page).to have_link 'マイページ'
    click_link "ログアウト"
    expect(page).to have_current_path "/"
    expect(page).to have_link 'ログイン'
    expect(page).to have_link 'ログアウト', count: 0
    expect(page).to have_link 'マイページ', count: 0
  end 
  
  it "remember_meにチェックを入れる" do
    visit root_url
    click_link "ログイン"
    fill_in "ニックネーム", with: "foobar"
    fill_in "パスワード", with: "foobar"
    check '次回から自動的にログインする'
    expect(page).to have_checked_field('次回から自動的にログインする')
    click_button "ログイン"
    expect(page).to have_current_path "/users/#{@user.id}"
    expect(@user.reload.remember_digest).to_not eq nil 
  end
end
