require 'rails_helper'

RSpec.feature "UsersIndex", type: :feature do
  before do
    @user = FactoryBot.create(:user, :foobar)
  end
  it "pagenationが機能している" do
    num = 0
    n = 0
    while num < 31 do
      FactoryBot.create(:user, nickname: "hogehoge#{n}")
      num = num + 1
      n += 1
    end
    log_in_as_foobar
    find('.dropdown-toggle').click
    click_link 'おすすめユーザー'
    expect(page).to have_link "hogehoge", count: 29 
    expect(page).to have_link 'Next →', count: 2
  end
end
