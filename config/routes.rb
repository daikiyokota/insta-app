Rails.application.routes.draw do
  get 'sessions/new'
  root 'static_pages#home'
  get  '/policy', to: 'static_pages#policy'
  get '/signup', to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get '/likes/:id', to: 'likes#show'
  post '/likes/:id', to: 'likes#create'
  delete '/likes/:id/destroy', to: 'likes#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts,          only: [:index, :new, :show, :create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :comments,            only: [:create, :destroy]
  resources :alerts,              only: [:show, :destroy]
  
end
